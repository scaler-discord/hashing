// Problem link: https://www.interviewbit.com/problems/subarray-with-given-xor/

int Solution::solve(vector<int> &A, int B) {
    unordered_map<int, int> mp;
    mp[0] = 1;
    int xr = 0;
    long long ans = 0;
    for(int x : A) {
        // Let's find no. of subarrays ending at x
        xr ^= x;
        ans += mp[xr^B];
        ++mp[xr];
    }
    return ans;
}
