// Problem link: https://www.interviewbit.com/problems/valid-sudoku/

bool checkRows(const vector<string>& a) {
    for(int row = 0; row < 9; row++) {
        vector<bool> used(9, false);
        for(int col = 0; col < 9; col++) {
            if(a[row][col] == '.') continue;
            int pos = a[row][col] - '1';
            if(used[pos]) return false;
            used[pos] = true;
        }
    }
    return true;
}

bool checkCols(const vector<string>& a) {
    for(int col = 0; col < 9; col++) {
        vector<bool> used(9, false);
        for(int row = 0; row < 9; row++) {
            if(a[row][col] == '.') continue;
            int pos = a[row][col] - '1';
            if(used[pos]) return false;
            used[pos] = true;
        }
    }
    return true;
}

bool checkBlocks(const vector<string>& a) {
    for(int row = 0; row < 9; row += 3) {
        for(int col = 0; col < 9; col += 3) {
            vector<bool> used(9, false);
            for(int r_offset = 0; r_offset < 3; r_offset++) {
                for(int c_offset = 0; c_offset < 3; c_offset++) {
                    if(a[row+r_offset][col+c_offset] == '.') continue;
                    int pos = a[row+r_offset][col+c_offset] - '1';
                    if(used[pos]) return false;
                    used[pos] = true;
                }
            }
        } 
    }
    return true;
}

int Solution::isValidSudoku(const vector<string> &A) {
    return checkRows(A) && checkCols(A) && checkBlocks(A);
}
