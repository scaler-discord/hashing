// Problem link: https://www.interviewbit.com/problems/anagrams/

string encode(vector<int>& f) {
    string ans = "";
    for(int i = 0; i < 26; i++) {
        ans += (f[i]/120);
        ans += (f[i]%120);
    }
    return ans;
}

vector<vector<int> > Solution::anagrams(const vector<string> &A) {
    unordered_map<string, vector<int>> mp;
    for(int i = 0; i < A.size(); i++) {
        vector<int> f(26, 0);
        for(char c : A[i]) ++f[c-'a'];
        string code = encode(f);
        mp[code].push_back(i+1);
    }
    vector<vector<int>> ans;
    for(auto& p: mp) ans.push_back(p.second);
    return ans;
}
