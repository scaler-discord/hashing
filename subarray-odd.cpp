// Problem link: https://www.interviewbit.com/problems/subarray-with-b-odd-numbers/

int Solution::solve(vector<int> &A, int B) {
    unordered_map<int, int> mp;
    mp[0] = 1;
    long long ans = 0, odds = 0;
    for(int x : A) {
        if(x&1) odds++;
        ans += mp[odds-B];
        ++mp[odds];
    }
    return ans;
}
