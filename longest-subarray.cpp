// Problem link: https://www.interviewbit.com/problems/longest-subarray-length/

int Solution::solve(vector<int> &A) {
    unordered_map<int, int> mp;
    mp[0] = -1;
    int diff = 0, ans = 0;
    for(int i = 0; i < A.size(); i++) {
        if(A[i] == 0) --diff;
        else ++diff;
        if(mp.find(diff-1) != mp.end()) ans = max(ans, i-mp[diff-1]);
        if(mp.find(diff) == mp.end()) mp[diff] = i; // update only if we haven't seen this diff before, so that we find the longest subarray
    }
    return ans;
}
